# feather-brief-webapp

## Description

For a full description of the project you should go [here](https://cn-ds.gitlab.io/blog/rex/2018/08/11/feather-brief-overview.html)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```
