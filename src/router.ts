import Vue from 'vue';
import Router from 'vue-router';

import Home from './views/Home.vue';
import Feeditems from './views/Feeditems.vue';
import Feeds from './views/Feeds.vue';
import Login from './views/Login.vue';
import Article from './views/Article.vue';
import Signup from './views/Signup.vue';
import Config from './views/Config.vue';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/feeditems',
      name: 'feeditems',
      component: Feeditems,
    },
    {
      path: '/feeds',
      name: 'feeds',
      component: Feeds,
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
    },
    {
      path: '/stats',
    },
    {
      path: '/article/:url',
      name: 'article',
      component: Article,
    },
    {
      path: '/signup',
      name: 'signup',
      component: Signup,
    },
    {
      path: '/config',
      name: 'config',
      component: Config,
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue'),
    },
  ],
});
