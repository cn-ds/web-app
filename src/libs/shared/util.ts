import { Config } from './config';
import axios from 'axios';

export class Util {

    static connect(user : String, pass : String, callback : Function) {
        return new Promise((resolve, reject) => {
            axios.post(Config.API_ENDPOINT + '/login', {
                username: user,
                password: pass
            })
            .then((response) => {
                resolve(callback(true, response.data.auth, response.data.token));
            })
            .catch(() => {
                callback(false, false);
            });
        });
    }
}