import { Feed } from '../../models/feed';
import { ApiProvider } from '../api/api';

export class FeedsProvider {

  api : ApiProvider;

  constructor() {
    this.api = new ApiProvider();
  }

  subscribe(feed : Feed) {
    this.api.post(`/feed?url=${feed.url}&name=${feed.name}`, {}, true, () => {});
  }

  unsubscribe(feed : Feed) {
    this.api.delete(`/feed?url=${feed.url}`, {}, true, () => {});
  }

  getFeeds(callback : Function) {
    this.api.getJSON('/feed', true, (feedsData : Array<Feed>, err : Boolean) => {
      if (!err) {
        let feeds = feedsData.map((feed : Feed) => {
          return new Feed(feed.name, feed.url);
        });
        callback(feeds, false);
      } else {
        callback([], true);
      }
    });
  }
}