import { ApiProvider } from '../api/api';
import { callbackify } from 'util';

export class AccountProvider {
  api : ApiProvider;

  constructor() {
    this.api = new ApiProvider();
  }

  register(email : String, username : String, password : String, callback : Function) {
    var data = {
      email,
      username,
      password
    }
    this.api.post('/register', data, false, callback);
  }

  importFile(formData : FormData, callback : Function) {
    this.api.filePost('/import/opml', formData, callback);
  }
}