import { Feeditem } from '../../models/feeditem';
import { ApiProvider } from '../api/api';

export class FeeditemsProvider {

  api : ApiProvider;

  constructor() {
    this.api = new ApiProvider();
  }
  
  parseFeeditems(jsonFeedArray : Array<Feeditem>) {
    return jsonFeedArray.map(elt => {
      new Feeditem(
        elt.article_url,
        elt.title,
        elt.feed_parent,
        elt.author,
        elt.description,
        elt.date,
        elt.summary,
        elt.read,
        false,
        elt.score
      )
    });
  }

  getFakeFeeditems() : Array<Feeditem> {
    let fakeFeed = new Feeditem("", "You're not connected", "Pull top to refresh once connected", "", "", 0, "", 0, false, 0);
    let feeditems : Array<Feeditem> = [];
    feeditems.push(fakeFeed);
    return feeditems;
  }

  getUnreadFeeditems(callback: Function, maxTime?:Number) {
    let maxTimeVal = maxTime ? maxTime : Number.MAX_SAFE_INTEGER;
    this.api.getJSON(`/feeditems?maxTime=${maxTimeVal}`,true, (feedItems : Array<Feeditem>, err : Boolean) => {
      if(!err) {
        callback(feedItems);
      } else {
        callback(this.getFakeFeeditems());
      }
    });
  }

  getUnreadFeeditemsByScore(callback: Function) {
    this.api.getJSON(`/feeditems?sortByScore=true`,true, (feedItems : Array<Feeditem>, err : Boolean) => {
      if(!err) {
        callback(feedItems);
      } else {
        callback(this.getFakeFeeditems());
      }
    });
  }

  getUnreadFeeditemsByFeed(feedname : String, callback : Function) {
    this.api.getJSON(`/feeditems?feed=${feedname}&offset=0`,true, (feedItems : Array<Feeditem>, err : Boolean) => {
      if(!err) {
        callback(feedItems);
      } else {
        callback(this.getFakeFeeditems());
      }
    });
  }

  markAsShown(feeditems : String[], callback : Function) {
    let data = {
      feedItems : feeditems
    }
    this.api.post(`/feeditems/shown`, data, true, callback);
  }
}