export class Feeditem{
	constructor(
		public article_url: string,
		public title: string,
		public feed_parent: string,
		public author: string,
		public description: string,
		public date: number,
		public summary: string,
		public read: number,
		public shown: boolean,
		public score: number
		) { }
}